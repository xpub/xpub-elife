# ⚠️  THIS REPOSITORY IS OUT OF DATE!

WORK IN THIS REPOSITORY HAS CEASED.

ELIFE-XPUB HAS MOVED TO https://github.com/elifesciences/elife-xpub.

---

## Quickstart

```
npm start # or docker-compose up
```

**Note**: yarn will be run automatically inside the container to install dependencies. If dependencies were already installed on the host, this may cause problems as the binaries are compiled for the wrong platform. If you encounter errors about "invalid ELF header", do `npm run clean` and then `npm start` again.

See `pubsweet-cli` for detailed documentation on running the app

## Installing

In the root directory, run `yarn` to install all the dependencies.

## Configuration

To enable manuscript conversion via INK, add the following values to `config/local-development.json` (ask in [the xpub channel](https://mattermost.coko.foundation/coko/channels/xpub) if you need an account):

```json
{
  "pubsweet-server": {
    "secret": "__EDIT_THIS__"
  },
  "pubsweet-component-ink-backend": {
    "inkEndpoint": "__EDIT_THIS__",
    "email": "__EDIT_THIS__",
    "password": "__EDIT_THIS__"
  }
}
```

## Running the app

1. The first time you run the app, initialise the database with `yarn run setupdb` (press Enter when asked for a collection title, to skip that step).
2. `yarn start`

## CI

CI requires a Kubernetes cluster, resources for which can be found in [`pubsweet/infra`](https://gitlab.coko.foundation/pubsweet/infra). In order to set up a Kubernetes cluster (using AWS) you need to follow the instructions there. Templates for deploying to this cluster with [`pubsweet/deployer`](https://gitlab.coko.foundation/pubsweet/deployer) are located in [`xpub/deployment-config`](https://gitlab.coko.foundation/xpub/deployment-config).

## Community

Join [the Mattermost channel](https://mattermost.coko.foundation/coko/channels/xpub) for discussion of xpub.
